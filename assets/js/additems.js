$(function() {
    var i = Number(localStorage.getItem('todo-counter')) + 1,
        j = 0,
        k,
        $form = $('#number'),
        $removeLink = $('#show-items li a'),
        $itemList = $('#show-items'),
        $editable = $('.editable'),
        $clearAll = $('#clear-all'),
        $newName = $('#name'),
        $newNumber = $('#phone'),
        order = [],
        orderList;
 
    // Load todo list
    orderList = localStorage.getItem('todo-orders');
     
    orderList = orderList ? orderList.split(',') : [];
     //console.log(localStorage);

    for( j = 0, k = orderList.length; j < k; j++) {
     var numbersplit = orderList[j].split('todo-');
     var number = 'number-'+numbersplit[1];
	 //console.log(number);
	 //console.log("-------------");
        $itemList.append(
            "<li id='" + orderList[j] + "'>"
        	    + "<div class='name'><img src='assets/images/user.png' alt='User Image' /><span class='editable'>"
    	        + localStorage.getItem(orderList[j]) 
	            + "</span></div><div class='phone'><img src='assets/images/iphone.png' alt='Phone Image' /><span class='editable'>"
                +localStorage.getItem(number) 
                + " </span></div><div class='bin'><a href='#'><img src='assets/images/bin.png' alt='Delete Entry image' /></a></div></li>"
            );

    }
         
    // Add contact
    $form.submit(function(e) {
        e.preventDefault();
        $.publish('/add/', []);
    });
 
    // Remove contact
    $itemList.delegate('a', 'click', function(e) {
        var $this = $(this);
         
        e.preventDefault();
        $.publish('/remove/', [$this]);
    });
     
    // Sort contact
    $itemList.sortable({
        revert: true,
        stop: function() {
            $.publish('/regenerate-list/', []);
        }
    });
     
    // Edit and save contact
    $editable.inlineEdit({
        save: function(e, data) {
                var $this = $(this);
                localStorage.setItem(
                    $this.parent().parent().attr("id"), data.value
                );
            }
    });
 
    // Clear all
    $clearAll.click(function(e) {
        e.preventDefault();
        $.publish('/clear-all/', []);
    });
          
    // Subscribes
    $.subscribe('/add/', function() {
        
    
//validate name
var name = $newName.val();
if (name.length < 3)
{
	$('.invalid').remove();
    $('.entryAREA').after('<div class="invalid">Please enter a name 3 characters or more.</div>');
    $('.invalid').effect('shake');
     return false;
}

//validate phone
var phone = $newNumber.val(),
    intRegex = /[0-9 -()+]+$/;
if((phone.length < 9) || (!intRegex.test(phone)))
{
	$('.invalid').remove();
    $('.entryAREA').after('<div class="invalid">Please enter a valid phone number.</div>');
    $( ".invalid" ).effect( "shake" );
     return false;
}
    ///Start adding in the name
    
        if ($newName.val() !== "") {
            // Take the value of the input field and save it to localStorage
            localStorage.setItem( 
                "todo-" + i, $newName.val()
            );
            localStorage.setItem( 
                "number-" + i, $newNumber.val()
            );     
            // Set the to-do max counter so on page refresh it keeps going up instead of reset
            localStorage.setItem('todo-counter', i);
            
            $('.invalid').remove();
 
            // Append a new list item with the value of the new todo list
            $itemList.append(
            
                "<li id='todo-" + i + "'>"
                + "<div class='name'><img src='assets/images/user.png' alt='User Image' /><span class='editable'>"
                + localStorage.getItem("todo-" + i)
                +"</span></div><div class='phone'><img src='assets/images/iphone.png' alt='Phone Image' /><span class='editable'>"
                + localStorage.getItem("number-" + i) 
                + " </span></div><div class='bin'><a href='#'><img src='assets/images/bin.png' alt='Delete Entry image' /></a></div></li>"
            );
            
            
 
            $.publish('/regenerate-list/', []);
 
            // Hide the new list, then fade it in for effects
            $("#todo-" + i)
                .css('display', 'none')
                .fadeIn();
             
            // Empty the input field
            $newName.val("");
            $newNumber.val("");
             
            i++;
        }
    });
    

     
    $.subscribe('/remove/', function($this) {
        var parentId = $this.parent().parent().attr('id');
         
        // Remove todo list from localStorage based on the id of the clicked parent element
        localStorage.removeItem(
            "'" + parentId + "'"
        );
         
        // Fade out the list item then remove from DOM
        $this.parent().parent().fadeOut(function() { 
            $this.parent().parent().remove();
             
            $.publish('/regenerate-list/', []);
        });
    });
     
    $.subscribe('/regenerate-list/', function() {
        var $todoItemLi = $('#show-items li');
        // Empty the order array
        order.length = 0;
         
        // Go through the list item, grab the ID then push into the array
        $todoItemLi.each(function() {
            var id = $(this).attr('id');
            order.push(id);
        });
         
        // Convert the array into string and save to localStorage
        localStorage.setItem(
            'todo-orders', order.join(',')
        );
    });

     
    $.subscribe('/clear-all/', function() {
        var $todoListLi = $('#show-items li');
         
        order.length = 0;
        localStorage.clear();
        $todoListLi.remove();
    });
});
